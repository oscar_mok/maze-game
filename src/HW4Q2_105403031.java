import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/*
 * 資管4A
 * 105403031  莫智堯
 */

public class HW4Q2_105403031 extends JFrame {

    private ImageIcon heart;
    private ImageIcon diamond;
    private ImageIcon brickwall;
    private JPanel main_JPanel;
    private File map_file;
    private Scanner scanner;
    private List<Integer> list = new LinkedList<>();
    private JProgressBar hp;

    public HW4Q2_105403031(){
        super();

        //產生血條
        hp = new JProgressBar(0, 100); //血條範圍0 ~ 100
        hp.setValue(100); //血條起始值：100
        //

        main_JPanel = new JPanel();
        main_JPanel.setLayout(new GridLayout(10, 10)); //分割成100格

        try {

            //讀入檔案
            map_file = new File("src/map.txt");
            scanner = new Scanner(map_file);
            //

            while (scanner.hasNext()){

                list.add(Integer.parseInt(scanner.next())); //把檔案所有資料都放到list裡面

            }

            //隨機改變牆壁 -> 愛心
            for (int j = 0; j < 10; j++){

                int random_num = (int) (Math.random() * 100); //產生0 ~ 100的隨機數，把隨機數暫存到random_num

                if (list.get(random_num) == 1){ //list 的隨機數位置value == 1（代表是牆壁）
                    list.set(random_num, 5); //那個value 改寫成5
                }
            }
            //

            //讀入png檔
            brickwall = new ImageIcon("src/brickwall.png");
            diamond = new ImageIcon("src/diamond.png");
            heart = new ImageIcon("src/heart.png");
            //

            //限制圖片大小
            brickwall.setImage(brickwall.getImage().getScaledInstance(60, 60, Image.SCALE_DEFAULT));
            diamond.setImage(diamond.getImage().getScaledInstance(60, 60, Image.SCALE_DEFAULT));
            heart.setImage(heart.getImage().getScaledInstance(60, 60, Image.SCALE_DEFAULT));
            //

            //Because imageicon cannot add directly by "add". Therefore, have to packed as a JLabel

            for (int i = 0; i < list.size(); i++){

                int temp = list.get(i);

                if (temp == 0){
                    //設定為通道
                    road();

                }else if (temp == 1){
                    //設定為牆
                    brickwall();

                }else if (temp == 5) {
                    //設定成愛心
                    heart();

                }else {
                    //設定為出口
                    diamond();

                }
            }

            add(main_JPanel, BorderLayout.CENTER);
            add(hp, BorderLayout.NORTH);

        } catch (FileNotFoundException e) {

            System.err.println("Error while reading file.");

        } catch (NumberFormatException e){

            System.err.println("Convert to int error.");

        }
    }

    private void diamond() {
        JLabel diamond_JLabel = new JLabel(diamond); //ImageIcon 不能直接add 進JPanel裡，所以要用JLabel包裝

        main_JPanel.add(diamond_JLabel);

        diamond_JLabel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                if (hp.getValue() > 0) {
                    JOptionPane.showConfirmDialog(getParent(), "You win!!");
                }else {
                    JOptionPane.showConfirmDialog(getParent(), "You lose! (hp is zero)");
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

    private void heart() {

        JLabel heart_JLabel = new JLabel(heart); //把愛心的圖片包到 JLabel裡

        main_JPanel.add(heart_JLabel); //每執行一次就把愛心加到JPanel 的格子裡

        heart_JLabel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                hp.setValue(hp.getValue() +  15); //碰到愛心血條加15hp
            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

    private void brickwall(){

        JLabel brickwall_JLabel = new JLabel(brickwall); //把牆的圖片包到 JLabel裡

        main_JPanel.add(brickwall_JLabel); //每執行一次就把牆加到JPanel 的格子裡

        brickwall_JLabel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                hp.setValue(hp.getValue() -  20); //每碰到一次血條就減20hp
            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

    private void road(){
        JLabel road_JLabel = new JLabel(); //空的JLabel 道路不用imageicon

        main_JPanel.add(road_JLabel); //每執行一次就加到JPanel 的格子裡

        road_JLabel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                hp.setValue(hp.getValue() -  5); //每碰到一次血條就減5hp
            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

}
