import javax.swing.*;

/*
 * 資管4A
 * 105403031  莫智堯
 */
public class Main extends HW4Q2_105403031{

    public static void main(String[] args){

        HW4Q2_105403031 Q2_drawer = new HW4Q2_105403031();
        Q2_drawer.setSize(600, 750);
        Q2_drawer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Q2_drawer.setVisible(true);

    }
}
